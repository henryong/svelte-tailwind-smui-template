/* eslint-disable import/no-extraneous-dependencies */
const tailwindcss   = require("tailwindcss");
const cssnano       = require("cssnano");
const purgecss      = require("@fullhuman/postcss-purgecss");
const autprefixer   = require("autoprefixer");
const postcssImport = require("postcss-import")();

module.exports = {
  plugins: [
    postcssImport,
    tailwindcss("./tailwind.config.js"),
    autprefixer,
    ...(process.env.NODE_ENV === "production"
      ? [cssnano("./cssnano.config.js"), purgecss("./purgecss.config.js")]
      : [])
  ]
};
