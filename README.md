# Svelte Personal Project
A template for personal project using SvelteJS, TailwindCSS, and Svelte-Material-UI.

## The stack consist of these main key libraries:
    - Svelte JS
        > Extrememly fast and lightweight front-end framework that compiles to pure HTML, CSS, and JavaScript.
        > https://svelte.dev/
    - Tailwind CSS
        > A utility-first CSS framework for rapidly building custom designs. Alternitive to Bootstrap.
        > https://tailwindcss.com/
    - Svelte Material UI
        > A library of Svelte 3 Material UI components, based on the Material Design Web Components.
        > https://github.com/hperrin/svelte-material-ui
    - Rollup
        > Alternative solution to Webpack or Bundle build by Rich Harris, the creator of Svelte.
        > https://rollupjs.org/guide/en/
    - SCSS
        > A CSS compiler that allows you to write logic into CSS.
        > https://sass-lang.com/


## Run the following NPM command in bash/powershell:
    - Prototyping and rapid-development : $ npm run build:dev
    - Production. Uglify and Minify     : $ npm run build:pro
    
## To run a local server with the builds:
    - Development : $ npm run dev
    - Production  : $ npm run pro
    

## Progress:
    - used a day to do
    - a work in progress
    
    
