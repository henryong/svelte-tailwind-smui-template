import svelte         from "rollup-plugin-svelte";
import resolve        from "rollup-plugin-node-resolve";
import commonjs       from "rollup-plugin-commonjs";
import livereload     from "rollup-plugin-livereload";
import postcss        from "rollup-plugin-postcss";
import autoPreprocess from "svelte-preprocess";
import {terser}       from "rollup-plugin-terser";

const production = !process.env.ROLLUP_WATCH;

export default {
  input: "src/main.js",
  output: {
    sourcemap: true,
    format: "iife",
    name: "app",
    file: "public/bundle.js"
  },
  plugins: [
    postcss({
      extract: "public/material.css",
      minimize: production,
      use: [
        ["sass", {
          includePaths: [
            "./src/styles",
            "./node_modules",
          ]
        }]
      ],
    }),
    svelte({
      preprocess: autoPreprocess({ postcss: true }),
      dev: !production,
      css: css => css.write("public/bundle.css"),
      //! The emnitCss portion was the one the created the problem of other
      //! "global" CSS files being written with Svelte CSSes.
      emitCss: !!0
    }),
    resolve({
      browser: true,
      dedupe: importee =>
        importee === "svelte" || 
        importee.startsWith("svelte/")
    }),
    commonjs(),
    !production && livereload("public"),
    production && terser()
  ],
  watch: { clearScreen: !0 }
};
