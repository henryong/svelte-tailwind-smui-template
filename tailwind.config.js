const gridConfig = require("./tailwind_plugins/tw-grid.config");
const grid       = require("./tailwind_plugins/tw-grid");
const card       = require("./tailwind_plugins/tw-card");

module.exports = {
  // prefix: "tw-",
  theme: {
    container: {
      center: true
    },
    extend: {}
  },
  variants: {},
  corePlugins: {
    preflight: !0
  },
  plugins: [grid(gridConfig), card]
};
