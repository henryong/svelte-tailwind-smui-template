/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint func-names: ["error", "never"] */
module.exports = function ({addComponents, theme}) {
  // get from default TailwindCss theme.
  const themeColors = theme("colors", {});
  const cardColorClasses = {};
  for (const color in themeColors) {
    if (typeof themeColors[color] === "string") {
      if (color === "transparent") {
        cardColorClasses[".card"] = {
          "border": "0 2px 2px 2px solid",
          "border-radius": "0.25rem",
          "border-color": `${themeColors[color]}`,
          "background-color": themeColors[color],
          "width": "100%",
          "overflow": "hidden",
          "box-shadow":
            "0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)"
        };
      } else {
        cardColorClasses[`.card-${color}`] = {
          "border-color": `${themeColors[color]}`,
          "background-color": themeColors[color]
        };
      }
    } else if (Object.keys(themeColors[color] !== undefined)) {
      for (const num in themeColors[color]) {
        const literColor = parseInt(num, 10) + 200;
        const colorNum = literColor <= 900 ? literColor : 900;
        cardColorClasses[`.card-${color}-${num}`] = {
          "border-color": theme(`colors.${color}.${colorNum}`),
          "background-color": themeColors[color][num]
        };
      }
    }
  }
  // add the Card class component.
  addComponents([
    {
      // .card
      ...cardColorClasses

      // ".card-image": {
      //   display: "block",
      //   width: "100%",
      // },

      // ".card-content": {
      //   padding: cardTheme.padding,
      // },
    }
  ]);
};
